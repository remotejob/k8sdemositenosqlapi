package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/rs/cors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gopkg.in/mgo.v2/bson"
)

var (
	client *mongo.Client
	err    error
)

func init() {

	client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://root:password123@mongodb:27017"))
	if err != nil {
		log.Println(err)
		// panic(err)
	}

	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.Println(err)
		// panic(err)
	} else {

		log.Println("Ping OK")
	}

}


type Employee struct {
	Employeeid int64
	EmployeeName string
	EmployeePhone string
}

func main() {

	mux := http.NewServeMux()

	mux.HandleFunc("/nosql/api", func(w http.ResponseWriter, r *http.Request) {

		employes := []Employee{}

		db := client.Database("test")

		collection := db.Collection("Employee")

		cursor, err := collection.Find(context.TODO(), bson.M{})
		if err != nil {
			log.Println(err)
		}
		for cursor.Next(context.TODO()) {
			var employee Employee
			
			cursor.Decode(&employee)
			employes =append(employes, employee )

		}

		w.Header().Set("Content-Type", "application/json")

		jsonResp, err := json.Marshal(employes)
		if err != nil {
			log.Println(err)
		}
		w.Write(jsonResp)

	})

	handler := cors.Default().Handler(mux)
	http.ListenAndServe(":9020", handler)
}

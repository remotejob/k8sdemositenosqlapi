# k8sdemositenosqlapi

## DOCS
https://medium.com/@pavelfokin/how-to-build-a-minimal-golang-docker-image-b4a1e51b03c8  
https://laurent-bel.medium.com/running-go-on-docker-comparing-debian-vs-alpine-vs-distroless-vs-busybox-vs-scratch-18b8c835d9b8  
https://medium.com/@maheshdere/use-delve-to-debug-go-code-instead-of-print-statement-770d68563892  
https://betterprogramming.pub/path-to-a-perfect-go-dockerfile-f7fe54b5c78c  
https://www.docker.com/blog/faster-multi-platform-builds-dockerfile-cross-compilation-guide/  
https://medium.com/@leonardo5621_66451/how-to-dockerize-a-go-application-d196ea292c34


## WORKFLOW
Simple REST service for MongoDB

## DB
```
db.Employee.insert(
	{
		"Employeeid" : 1,
		"EmployeeName" : "Martin",
        "EmployeePhone" : "+7347272722"
	}
)

db.Employee.insert(
	{
		"Employeeid" : 2,
		"EmployeeName" : "Alex",
        "EmployeePhone" : "+7347200000"
	}
)
db.Employee.insert(
	{
		"Employeeid" : 3,
		"EmployeeName" : "Olga",
        "EmployeePhone" : "+734721111111"
	}
)
```

## DOCKER
docker buildx use $(docker buildx create --platform linux/amd64,linux/arm64)  
docker buildx build -t "remotejob/k8sdemositenosqlapi:v1.0" -f Dockerfile.mongodb --platform linux/amd64,linux/arm64 --push .

## API Service
kubectl apply -f k8s/domain/cert-rollersoft-ml.yml  
kubectl apply -f k8s/api/corsheaders.yml  
kubectl apply -f  k8s/api/k8sdockemongodbervice.yml   
kubectl delete -f  k8s/api/k8sdockemongodbervice.yml    
kubectl rollout restart deployment k8sdemositenosqlapi-golang  

## Test
curl https://mongoapi.demoproject.one/nosql/api  




